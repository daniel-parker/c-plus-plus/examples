//
//  Animal.cpp
//  HelloWorld
//
//  Created by Daniel Parker on 17/2/2022.
//

#include <stdio.h>
#include "Animal.h"
#include <string>
#include <ostream>

// Constructors
Animal::Animal(): type(""), age(0) {}
Animal::Animal(std::string type, unsigned short int age): type(type), age(age) {}

// Destructor
Animal::~Animal() {}

// Output operator
std::ostream& operator<<(std::ostream& os, const Animal& animal) {
    os << "Type: " << animal.type << ", Age: " << animal.age << std::endl;
    
    return os;
}
