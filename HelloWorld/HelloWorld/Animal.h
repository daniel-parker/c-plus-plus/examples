//
//  Animal.h
//  HelloWorld
//
//  Created by Daniel Parker on 17/2/2022.
//
#pragma once

#include <stdio.h>
#include <string>

class Animal {
public:
    // Constructors
    Animal();
    Animal(std::string type, unsigned short int age);
    
    // Destructor
    virtual ~Animal();
    
    // To make printing the object easier
    friend std::ostream& operator<<(std::ostream& os, const Animal& animal);
    
private:
    // Members
    std::string type;
    unsigned short int age;
};
