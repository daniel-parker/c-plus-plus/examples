//
//  main.cpp
//  HelloWorld
//
//  Created by Daniel Parker on 17/2/2022.
//

#include "Animal.h"
#include <iostream>

int main(int argc, const char * argv[]) {
    Animal animal1("Dog", 10);
    
    std::cout << animal1;
    
    return 0;
}
